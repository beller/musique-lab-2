Accompagné de nombreux exemples musicaux et de séquences pédagogiques, Musique Lab 2 permet d’explorer les paramètres du son, de manipuler des matériaux musicaux ou encore de reconstruire des fragments d'œuvres complexes comme dans l’atelier d’un compositeur. Les projets de créations des élèves tirent également parti de nombreux opérateurs pour générer, analyser ou transformer des matériaux musicaux, sous forme symbolique, Midi ou audio. 

Musique Lab 2 permet aux enseignants de mettre en œuvre des dispositifs pédagogiques innovants pour leurs élèves, en vue de :

Favoriser le lien entre connaissances musicales théoriques et approche pratique par l’expérimentation
Décrypter et comprendre les œuvres en entrant dans l’atelier de composition
Soutenir la créativité dans l’apprentissage et initier des projets de création